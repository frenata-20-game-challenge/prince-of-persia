extends Area2D

signal event(cell_pos)

var cell_pos
var listens = [] # NOTE: change state if one of these cells is activated

func new(pos, trigger, listens):
	cell_pos = pos
	trigger.connect(handle_trigger)
	self.listens = listens
	return self

func player_enter():
	get_parent().set_cell(1, cell_pos, 5, Vector2(0,0))
	event.emit(cell_pos)
	
func player_exit():
	get_parent().erase_cell(1, cell_pos)
	get_parent().set_cell(1, cell_pos, 5, Vector2(0,2))

func handle_trigger(cell_pos):
	for listen in listens:
		if cell_pos == listen.target:
			print("triggered")
			player_enter()
