extends Area2D

signal event(cell_pos)

var cell_pos
var listens = [] # NOTE: change state if one of these cells is activated
var active = true

func new(pos, trigger, listens):
	cell_pos = pos
	trigger.connect(handle_trigger)
	self.listens = listens
	return self

func player_enter():
	if active:
		get_parent().set_cell(1, cell_pos, 5, Vector2i(0,1))
		%Trigger.start(.5)
	
func player_exit():
	if active:
		get_parent().set_cell(1, cell_pos, 5, Vector2i(0,0))
		%Trigger.stop()

func drop():
	active = false
	get_parent().erase_cell(1, cell_pos)
	event.emit(cell_pos)
	# TODO: create falling debris
	queue_free()

func trigger_drop():
	%Drop.start()

func handle_trigger(cell_pos):
	for listen in listens:
		if cell_pos == listen.target:
			print("triggered")
			player_enter()
