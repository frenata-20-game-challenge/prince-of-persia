extends Node

signal trigger(tile)

func event(tile_pos):
	print("event triggered from: ", tile_pos)
	trigger.emit(tile_pos)
