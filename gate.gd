extends StaticBody2D

var state = "down"
var speed = 1
var default_size
var default_pos

var cell_pos
var listens = [] # NOTE: change state if one of these cells is activated


func _ready():
	default_size = %Shape.shape.size
	default_pos = %Shape.position

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if state == "raising":
		var shape = %Shape.shape.size
		var pos = %Shape.position
		%Shape.shape.size = Vector2(shape.x, shape.y - (2 * speed))
		%Shape.position = Vector2(pos.x, pos.y- (1.5*speed))
	elif state == "dropping":
		var shape = %Shape.shape.size
		var pos = %Shape.position
		%Shape.shape.size = Vector2(shape.x, shape.y - (2*speed))
		%Shape.position = Vector2(pos.x, pos.y- (1.5*speed))

func raise(rate=1):
	speed = rate
	print("raise")
	state = "raising"
	if %Sprite.is_playing():
		%Sprite.pause()
	%Sprite.play("default", rate)

func fall(rate=-1):
	speed = rate
	if %Timer.timeout.is_connected(fall):
		%Timer.timeout.disconnect(fall)
	print("fall")
	state = "dropping"
	if %Sprite.is_playing():
		%Sprite.pause()
	%Sprite.play("default", rate)

func _on_animation_finished():
	var shape = %Shape.shape.size

	if state == "raising":
		state = "up"
		%Shape.shape.size = Vector2(shape.x, 1)
		%Timer.start()
		if not %Timer.timeout.is_connected(fall.bind(-0.1)):
			%Timer.timeout.connect(fall.bind(-0.1))
	elif state == "dropping":
		state = "down"
		%Shape.shape.size = default_size
		%Shape.position = default_pos


func new(pos, trigger, listens):
	cell_pos = pos
	trigger.connect(handle_trigger)
	self.listens = listens
	return self


func handle_trigger(cell_pos):
	for listen in listens:
		if cell_pos == listen.target:
			if listen.payload == "down":
				fall(-20)
			elif listen.payload == "up":
				raise()
