extends TileMap

var panel_scene = preload("res://floor_plate.tscn")
var drop_scene = preload("res://floor_drop.tscn")
var gate_scene = preload("res://gate.tscn")

# NOTE: make the value more complex -- a list of triggering cells
# and the payloads each sends with the trigger.
# This will let some panels lift a gate and others drop it.
var triggers = {Vector2i(11,7): [{"target": Vector2i(8,7), "payload": "down"},
								 {"target": Vector2i(1,7), "payload": "up"}]} # NOTE: level-specific, pass this in

# Called when the node enters the scene tree for the first time.
func _ready():
	var panels = get_used_cells_by_id(1, 5, Vector2i(0,2))
	for cell in panels:
		print("create plate at ", cell)
		var panel = panel_scene.instantiate().new(cell, %Bus.trigger, triggers.get(cell, []))
		panel.event.connect(%Bus.event)
		panel.position = to_global(map_to_local(cell))
		add_child(panel)
		
	var drops = get_used_cells_by_id(1, 5, Vector2i(0,0))
	for cell in drops:
		print("create drop at ", cell)
		var drop = drop_scene.instantiate().new(cell, %Bus.trigger, triggers.get(cell, []))
		drop.event.connect(%Bus.event)
		drop.position = to_global(map_to_local(cell))
		add_child(drop)
		
	var gates = get_used_cells_by_id(1, 7, Vector2i(0,0))
	for cell in gates:
		print("create gate at ", cell)
		var gate = gate_scene.instantiate().new(cell, %Bus.trigger, triggers.get(cell, []))
		var pos = to_global(map_to_local(cell))
		gate.position = Vector2(pos.x, pos.y - 48)
		add_child(gate)
